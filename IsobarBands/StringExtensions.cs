﻿namespace System
{
    public static class StringExtensions
    {
        public static bool SearchContains(this string searched, string search)
        {
            if (searched == null || search == null)
            {
                return false;
            }
            return searched.ToLower().Contains(search.ToLower());
        }
    }
}