﻿using IsobarBands.DTO;
using IsobarBands.Models;
using IsobarBands.Services;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace IsobarBands.Controllers
{
    [RoutePrefix("api/bands")]
    public class BandsController : ApiController
    {
        readonly BandService _bandService;

        public BandsController()
        {
            _bandService = new BandService();
        }

        [HttpGet, Route("{id}")]
        public IHttpActionResult GetBand(string id)
        {
            Band band = _bandService.GetBand(id);
            return Ok(band);
        }

        [HttpGet, Route("")]
        public IHttpActionResult GetBands(string search = null, OrderBy orderBy = OrderBy.Alphabetic)
        {
            IEnumerable<BandDTO> bands = _bandService.GetBands(search, orderBy);
            return Ok(bands);
        }
    }
}