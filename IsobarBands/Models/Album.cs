﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace IsobarBands.Models
{
    public class Album
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("releasedDate")]
        public DateTime ReleasedDate { get; set; }

        [JsonProperty("band")]
        public string Band { get; set; }

        [JsonProperty("tracks")]
        public List<Track> Tracks { get; set; }
    }
}