﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace IsobarBands.Models
{
    public class Band
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("genre")]
        public string Genre { get; set; }

        [JsonProperty("biography")]
        public string Biography { get; set; }

        [JsonProperty("numPlays")]
        public int NumPlays { get; set; }

        [JsonProperty("albums")]
        public List<string> AlbumIds { get; set; }

        [JsonProperty("albumList")]
        private List<List<Album>> AlbumList { get; set; }

        public IEnumerable<Album> Albums
        {
            get
            {
                if (AlbumList != null)
                {
                    foreach (List<Album> albumList in AlbumList)
                    {
                        var album = albumList?.FirstOrDefault();
                        if (album != null)
                        {
                            yield return album;
                        }
                    }
                }
            }
        }
    }
}