﻿namespace IsobarBands.DTO
{
    public class BandDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public int NumPlays { get; set; }
    }
}