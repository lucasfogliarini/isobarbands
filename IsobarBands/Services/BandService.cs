﻿using IsobarBands.DTO;
using IsobarBands.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IsobarBands.Services
{
    public class BandService
    {
        readonly RecruitingBandsWebService _recruitingBandWebService;
        public BandService()
        {
            _recruitingBandWebService = new RecruitingBandsWebService();
        }
        public Band GetBand(string id)
        {
            var bands = _recruitingBandWebService.GetBands();
            return bands.FirstOrDefault(b => b.Id == id);
        }

        public IEnumerable<BandDTO> GetBands(string search, OrderBy orderBy)
        {
            var bands = _recruitingBandWebService.GetBands();
            var query = string.IsNullOrEmpty(search) ? bands : bands.Where(b => b.Name.SearchContains(search) ||
                            (b.Albums != null  && b.Albums.Any(a=>a.Name.SearchContains(search))));

            if (orderBy == OrderBy.Alphabetic)
            {
                query = query.OrderBy(b => b.Name);
            }
            else
            {
                query = query.OrderByDescending(b => b.NumPlays);
            }

            return query.Select(b=> new BandDTO()
            {
                Id = b.Id,
                Name = b.Name,
                Image = b.Image,
                NumPlays = b.NumPlays
            });             
        }
    }
}