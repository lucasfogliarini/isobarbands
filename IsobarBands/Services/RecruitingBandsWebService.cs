﻿using IsobarBands.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace IsobarBands.Services
{
    public class RecruitingBandsWebService
    {
        WebClient webClient = new WebClient();

        public IEnumerable<Band> GetBands()
        {
            string tempFilePath = GetTempFilePath();
            if (!File.Exists(tempFilePath))
            {
                webClient.DownloadFile("https://iws-recruiting-bands.herokuapp.com/api/full", tempFilePath);
            }

            string bandsJson = File.ReadAllText(tempFilePath);
            return JsonConvert.DeserializeObject<List<Band>>(bandsJson);
        }

        private string GetTempFilePath()
        {
            string tempFileName = "bands_" + DateTime.Today.ToString("dd-MM-yyyy") + ".json";
            return Path.Combine(Path.GetTempPath(), tempFileName);
        }
    }
}