﻿namespace IsobarBands.Services
{
    public enum OrderBy
    {
        Alphabetic,
        Popularity
    }
}