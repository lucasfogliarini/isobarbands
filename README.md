### Queries

Search Bands by alphabetic: http://localhost:12196/api/bands?search=Beat&orderBy=0

Search Bands by popularity: http://localhost:12196/api/bands?search=Led&orderBy=1

Get All Bands: http://localhost:12196/api/bands/

Get a Band: http://localhost:12196/api/bands/58c15e07a7ec6204001f0461
